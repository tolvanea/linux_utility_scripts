#!/bin/bash

# This script downloads youtube playlists to music. It uses a "youtube-dl"
# fork "yt-dlp" for that purpose.
# This script is essentially just an easier interface to the orignal command.
# Also, other websites are supported too, like soundcloud, for example.

# Alpi Tolvanen
# Licence: MIT or public domain

help_text="
Download youtube playlists as music. Uses OPUS file format. This script
simplifies usage of 'yt-dlp', which is a fork of 'youtube-dl'.

Usage:
    yt-dl [options] <url>

Options:
    -n, --no-autonumber               Disable autonumbering in filenames.
                                      By default name would be something like '12_video.m4a'.

    -r, --reverse                     Download playlist in flipped order. The order of plalist is
                                      sometimes reversed for no reason. Use this flag then.

    -a, --arguments  '--arg1 --arg2'  Pass arbitrary arguments to youtube-dl

    -t, --no-thumbnail                Do not set coverart from thumbnail. (Without thumbnail
                                      there is no need to install atomicparsley.)

    -v, --video                       Save as video. You could just call yt-dlp directly instead
                                      of this script. The only difference is that this script also
                                      adds metadata from the video description.

    -h, --help                        Print this help

Examples:
    Download youtube playlist to music:
        yt-dl https://www.youtube.com/playlist?list=PLelnDqtESEtqoujpEgLE6x5Q1ybJvrVJJ

    Download a single video to music (without autonumbering).
        yt-dl --no-autonumber https://www.youtube.com/watch?v=sWNKdT6tPG8

    Download a full channel as plain videos:
        yt-dl -v https://www.youtube.com/channel/UCT1tAbK9XgoSxAi2xUIqY4w

    Download all music from a soundcloud page:
        yt-dl https://soundcloud.com/magnetude

Installation:
    Install yt-dlp, on linux run:
        sudo curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp
        sudo chmod a+rx /usr/local/bin/yt-dlp
    Install a program that parses cover art to sound files:
        sudo apt-get install atomicparsley

    When youtube changes its APIs so that yt-dlp breaks, update it with:
        yt-dlp -U
"

# Main features of this script:
#   - Extract only audio from video and encode it in opus format
#   - Place video thumbnail to cover art
#   - Save a list of downloads to .txt file. If this scipt is called again in same directory,
#     it does not download the same videos again. This makes updating a playlist easy.
#   - Name videos with running numbering. For example "002 Cool song.opus"
#   - Download playlist in correct order, that is, start downloading from first video.

# These youtube-dl arguments can be tuned with this script
title="%(playlist_index)s_%(title)s.%(ext)s"  # Title with autonumbering. (It breaks without '_')
reverse="--playlist-reverse"                    # Playlist order
thumbnail="--embed-thumbnail"                   # Cover art
custom_arguments=""                             # Arguments to be passed to youtube-dl
#               | only audio    | AAC format       | Best quality
audio="--extract-audio --audio-format opus --audio-quality 0 "

# Print help
if [[ $# == 0 ]] || [[ $1 == "-h" ]] || [[ $1 == "--help" ]] || [[ $1 == "help" ]]; then
    echo "$help_text"
    exit
fi

# Parse arguments
while [[ $# -gt 1 ]]
do
key="$1"
case $key in
    -a|--arguments)                 # Pass arbitrary argument to youtube-dl
        custom_arguments=$2         # For example, to download only 10 most recent videos:
        shift                       #     yt-dl -a '--playlist-end 10' https://...
        ;;
    -n|-na|--no-autonumber)         # Turn off autonumber. Remember ot use this for singe videos
        title="%(title)s.%(ext)s"
        ;;
    -r|--reverse)                   # Flip order. The order of playlist may be totally opposite
                                    # depending on just playlist. Probably just a youtube-dl bug.
        reverse=""
        ;;
    -t|--no-thumbnail)              # Don't add thumbnail to audio
        thumbnail=""
        ;;
    -v|--video)                     # Don't convert to audio and save in video format
        audio=""
        thumbnail=""
        ;;
esac
shift # past argument or value
done

#python3 -m pip install --upgrade yt-dlp

# If required software is not installed then exit
err_msg="A 'youtube-dl' fork 'yt-dlp' is not installed. Install it with: \n\n
sudo curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp \n
sudo chmod a+rx /usr/local/bin/yt-dlp \n\n"
command -v yt-dlp >/dev/null 2>&1 || { echo >&2 $err_msg; exit 1;}
if [[ $thumbnail != "" ]] || [[ $audio == "" ]]; then
    err_msg="atomicparsley not installed. On ubuntu you can run 'sudo apt-get install atomicparsley'."
    command -v AtomicParsley >/dev/null 2>&1 || { echo >&2 $err_msg; exit 1; }
fi


#         | Easy playlist updating       | list order | autonumbering  | skip failed videos
settings="--download-archive archive.txt $reverse     -o $title        --ignore-errors "
#               |extraction | cover art | artist name
audio_settings="$audio      $thumbnail  --add-metadata"
url=$1

# Run the magic
yt-dlp $audio_settings $settings $custom_arguments $url


