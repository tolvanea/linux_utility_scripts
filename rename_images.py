#!/usr/bin/python3
"""
Renames all files matching "*.JPG" in current directorys with a date.
Uses exif-metadata in images to get the date. So if you have
images like "DSC01234.JPG", they gets renamed like "2022-04-22_13.JPG",
where the "13" is a running number for the day.

Usage:
    cd path/to/my_pictures
    rename_images.py
"""

import pyexiv2  # pip3 install py3exiv2
import os, math
from datetime import datetime
from dateutil import parser


# Get jpg-images in current directory as list. E.g. returns list
#       ["DSC45678.JPG", "IMG01234.JPG"]
def get_images():
    path_to_images = "."
    extensions =  [".jpg", ".jpeg", ".arw", ".dng"]
    files = []
    for f in sorted(os.listdir(path_to_images)):
        filepath = os.path.join(path_to_images, f)
        file_ext = os.path.splitext(filepath)[1]
        for matching_ext in extensions:
            # Case insensitive extension: Both "jpg" and "JPG" match
            # Can be with or without dot: Both ".jpg" and "jpg" match
            if file_ext.lower().endswith(matching_ext.lower()):
                files.append(filepath)
    return files

# Extracts dates from images using their exif-metadata.
# Group images to a dictionary by date.
#
# Input: List of file-names of pictures, for example:
#       ["DSC45678.JPG", "IMG01234.JPG"]
# Output: Dict<date, Set<(filename, datetime)>>, where
#       'date' is iso-date (string) e.g. "2022-08-14"
#       'filename' is filename (string) e.g. "DSC05730.JPG"
#       'datetime' is datetime-object that containing also the time
def get_iso_dates(filenames):

    # There are alternative formats for metadata: exif and xmp. This function
    # reads date from exif metadata.
    def read_exif_date(metadata, filename):
        # Few Exif codes and corresponding names for date field.
        # Some manufacturers use code as dict key, some use cleartext name.
        # Ps. Dear exif format engineers: Arbitrary codes is not how you should use a map/dictionary.
        date_codes = [36867, 306, 36868]
        date_names = ["Exif.Photo.DateTimeOriginal", "Exif.Photo.DateTime", "Exif.Photo.DateTimeDigitized"]

        all_keys = set(metadata.exif_keys)
        # Hopefully at least one date field is specified
        for date_key in date_codes + date_names:
            if date_key not in all_keys:
                continue
            # Whohoo, date field exists in metadata
            return datetime.strptime(metadata[date_key].raw_value, '%Y:%m:%d %H:%M:%S')
        else:
            return None

    # XMP is alternative format to exif. Some manufactures (canon) has only xmp available.
    def read_xmp_date(metadata, filename):
        all_keys = set(metadata.xmp_keys)
        date_name = "Xmp.xmp.CreateDate"

        if date_name in all_keys:
            return parser.isoparse(metadata[date_name].raw_value)
        else:
            return None

    iso_dates = {}
    for filename in filenames:
        metadata = pyexiv2.ImageMetadata(filename)
        metadata.read()

        if metadata is not None:

            date = read_exif_date(metadata, filename)
            if date is None:  # Exif not available, try XMP instead
                date = read_xmp_date(metadata, filename)

            if date is not None:
                # Date without time, e.g. "2022-08-14"
                iso_date = date.replace(microsecond=0).isoformat()[:-9]

                if iso_date not in iso_dates:
                    iso_dates[iso_date] = []
                iso_dates[iso_date].append((filename, date))
                continue # success obtaining date metadata

        print("Date metadata not found for image", filename)

    return iso_dates


# Given names of images and the time they are taken, rename them.
# Input:
#     iso_dates: See 'get_iso_dates' for type information
# Output:
#     None, but renames files as a side effect
def rename(iso_dates):
    for iso_date, old_names_and_times in iso_dates.items():

        name_collisions = []

        ordered = sorted(old_names_and_times, key=lambda pair: pair[1]) # numbering by time
        old_names = set([pair[0] for pair in old_names_and_times])

        for i, (old_name, date_string) in enumerate(ordered):
            extension = os.path.splitext(old_name)[1]
            images_of_day = len(old_names_and_times)

            # Only one image for the date: No numbering. E.g. "2022-08-14.JPG"
            if images_of_day == 1:
                new_name = iso_date + extension
            # Multiple images per date, use numbering. E.g. "2022-08-14_01.JPG, ..."
            else:
                digits = int(math.log10(images_of_day)) + 1
                new_name = "{}_{:0{}d}{}".format(iso_date, i+1, digits, extension)
                # Avoid overwriting if new name collides with the old. Rename with temporal prefix
                # "tmp_". This may happen if the script is used to rename already renamed files.
                if new_name in old_names:
                    name_collisions.append((new_name, old_name))
                    new_name = "tmp_" + new_name  # Assuming that no file begins with "tmp_"
                else:
                    print(f"Renamed {old_name} to {new_name}")

            os.rename(old_name, new_name)

        for new_name, old_name in name_collisions:  # When all have been renamed, handle the collided ones
            assert (not os.path.isfile(new_name))  # No overwrite should happen
            os.rename("tmp_" + new_name, new_name)
            print(f"Renamed {old_name} to tmp_{new_name} to {new_name}")

def main():
    filenames = get_images()
    iso_dates = get_iso_dates(filenames)
    rename(iso_dates)

main()

