import csv

# How much I have spent money on, e.g. bandcamp?
def total_by(filter, filename):
    sums = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        print("\nTransactions:")
        for row in csv_reader:
            if filter(row):
                currency = row[6]
                val = float(row[7].strip("\"").replace(",", ".").replace("−", "-").replace(" ", ""))
                if currency not in sums:
                    sums[currency] = 0.0
                sums[currency] += val
                title = row[15]
                print("{}: {}, {}".format(currency, val, title))
    print("-----------------------")
    print("Total sums by currency")
    for currency in sums:
        print("    {}: {}".format(currency, sums[currency]))
    print("----------------------------------------------")
    print("\n\n")

def main():
    filename = "2014-2021.csv"

    print("Total bandcamp")
    total_by(lambda row: row[3] == "Bandcamp, Inc.", filename)
    print("Total ebay")
    total_by(lambda row: row[4] == "eBay Auction Payment", filename)
    print("Total bank transfers")
    total_by(lambda row: row[4] == "Electronic Funds Transfer Funding", filename)
    print("Total all")
    types = ["General Currency Conversion", "Electronic Funds Transfer Funding"]
    total_by(lambda row: row[4] not in types, filename)

main()
