#!/usr/bin/env python3

# Read metadata of jpg or raw image. Also show some vendor specific fields
# Usage:
#   print_metadata.py <file>

# Installation of pyexiv2 on ubuntu:
#     sudo apt-get install libboost-python-dev libexiv2-dev python3-pip
#     pip3 install py3exiv2

# Documentation: https://python3-exiv2.readthedocs.io/en/latest/api.html#humanvalue
import pyexiv2
import sys
metadata = pyexiv2.ImageMetadata(sys.argv[1])
metadata.read()
for key in metadata.exif_keys:
    print(metadata[key])
