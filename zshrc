export ZSH="${HOME}/.oh-my-zsh"
ZSH_THEME="robbyrussell"
UPDATE_ZSH_DAYS=180

plugins=(
  # oh-my-zsh plugins
  git celery cp docker pip pylint rsync sudo ubuntu ufw virtualenv
  # custom downloaded plugins
  # git clone --depth 1 https://github.com/unixorn/fzf-zsh-plugin.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-zsh-plugin
  # sudo apt-get install silversearcher-ag
  #fzf
  # git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
  zsh-autosuggestions
  # git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
  zsh-syntax-highlighting
)
source $ZSH/oh-my-zsh.sh

#export FZF_DEFAULT_COMMAND='ag --hidden -g ""'

export LANG=en_US.UTF-8  # Fix remnant charactes getting stuck in prompt

# Unbind ctrl-c and ctrl-v to make room for copying and pasting.
# To actually enable copy paste with ^C and ^V, tweak terminal's setting for that.
# See https://askubuntu.com/a/477823
case $- in
    *i*)
        # Lift control sequence keys one keyrow upward to prevent accidental mispresses
        stty eof ^E     # ^D -> ^E  (make room for ^C -> ^D)
        stty intr ^D    # ^C -> ^D
        stty lnext ^F   # ^V -> ^F
        stty susp ^A    # ^Z -> ^A
        stty werase ^T  # ^W -> ^T  (make room for ^S -> ^W)
        stty stop ^W    # ^S -> ^W
        bindkey -r quoted-insert  #  unbind the quoted-insert function of zsh - free Ctrl+V for paste
esac

# Separate all commands with a newline
precmd() {
    echo
}

HISTSIZE=10000000
SAVEHIST=10000000

export PATH="${HOME}/.local/bin:${PATH}"

if [ -d "/home/alpi/PC" ]; then
    export PYTHONPATH="${PYTHONPATH}:/home/alpi/Documents/Työ/tyo/koodit/python/PIMC-automatization/"
    export OMP_NUM_THREADS=1

    export PATH="$PATH:/home/alpi/Documents/Tietsikka/scriptit_pathissa"
    export PATH="$PATH:/home/alpi/Documents/Tietsikka/linux_utility_scripts"
    export PATH="$PATH:/home/alpi/PC/ohjelmat"
    export PYTHONPATH="${PYTHONPATH}:/home/alpi/Documents/Tietsikka/ohjelmointi/python/kirjastot"
    
    # Print stderr with red. For more see https://github.com/sickill/stderred
    red_colored_text=$(tput setaf 9)
    export STDERRED_ESC_CODE=`echo -e "$red_colored_text"`
    export LD_PRELOAD="/home/alpi/PC/ohjelmat/stderred/build/libstderred.so${LD_PRELOAD:+:$LD_PRELOAD}"
elif [ -d "/mnt/c/Users/alpi.tolvanen" ]; then # WSL
    export PATH="$PATH:/mnt/c/Users/alpi.tolvanen/Documents/ohjelmointi/scriptit"
elif [ -d "/c/Users/alpi.tolvanen" ]; then # Git for windows
    export PATH="$PATH:/c/Users/alpi.tolvanen/Documents/ohjelmointi/scriptit"
elif [ -d "/home/${USER}/.cargo" ]; then
    export PATH="/home/${USER}/.cargo/bin/:${PATH}"
    export RUST_BACKTRACE=1
fi

findit() {
    # https://unix.stackexchange.com/questions/42841/how-to-skip-permission-denied-errors-when-running-find-in-linux
    if [[ $# -ne 1 ]]; then
        echo "findit: Search files and directories recursicely. Unlike 'find', it does not pollute output with errors."
        echo "Usage:    findit <some part of filename>"
        echo "Example:  findit 'my_file'"
    else
        find . -iname "*$1*"  2>&1 | grep -v "Permission denied" | grep -v "No such file or directory" | grep -v "Invalid argument"
    fi
}

pdfsearch() {
    # https://stackoverflow.com/questions/4643438/how-to-search-contents-of-multiple-pdf-files
    if [[ $# -ne 1 ]]; then
        echo "pdfsearch: Search content from multiple pdfs recursively. For example, search some word from directory of books."
        echo "Usage:    pdfsearch <text snippet>"
        echo "Example:  pdfsearch 'citation'"
    else
        find . -name '*.pdf' -exec sh -c "pdftotext \"{}\" - | grep -nH -B 1 -A 1 --label=\"{}\" --color \"$2\"" \;
    fi
}

replacerec() {
    # https://superuser.com/questions/422459/substitution-in-text-file-without-regular-expressions
    # https://stackoverflow.com/questions/1583219/how-to-do-a-recursive-find-replace-of-a-string-with-awk-or-sed
    if [[ $# -ne 3 ]]; then
        echo "replacerec: Find and replace text recursicely. Unlike 'sed' it does not try to match special symbols"
        echo "            with regex. However, backslashes must be escaped in replacing text, see third example."
        echo "            If your string contains special characters, use single hypens like: 'my #%/^ text '."
        echo "Usage:    replacerec '<old-text>' '<new-text>' '<filter>'"
        echo "Example:  replacerec '(^_^)' ':D' '*.txt'"
        # Note, when this line is printed for user, \\\\ is replaced with \\, which corresponds a single backslash \.
        printf '%s\n' "Example:  replacerec 'this is a single backslash: \\\\' 'new text with the backslash: \\\\' '*.tex'"
    elif ! command -v ruby &> /dev/null; then
        echo "Error: Ruby not installed. Install it with 'sudo apt-get install ruby'."
    else
        export FINDTHIS="$1"
        export REPLACE="$2"
        echo "Visiting following files:"
        find . \( -type d -name .git -prune \) -o -type f -name "$3" -exec echo {} \;  -exec \
            ruby -p -i -e "gsub(ENV['FINDTHIS'], ENV['REPLACE'])" {} \;
    fi
}

cut_video() {
    if [[ $# -ne 4 ]]; then
        echo "cut_video: Cut section of video without encoding."
        echo "Usage:    cut_video <start-time> <end-time> <input-file> <output-file>"
        echo "Example:  cut_video 25 35 in.mp4 out.mp4"
        echo "Example:  cut_video 2:25 2:35 in.mp4 out.mp4"
    else
        ffmpeg -ss "$1" -to "$2" -i "$3" -vcodec copy -acodec copy "$4"
    fi
}

concat_videos() {
    if [ $# -lt 1 ]; then
        echo "concat_videos: Concatenate two or more videos into one."
        echo "All videos must be the same format and size."
        echo "This may produce minor visual glitches to cuts."
        echo "Usage:        concat_videos <input 1> <input 2> ... <output>"
        echo "Example:      concat_videos a.mp4 b.mp4 c.mp4 output.mp4"
    else
        ARGS=("$@")         # arguments as an array
        output=${ARGS[-1]}  # last argument is output file name
        for f in "${ARGS[@]}"; do
            # Bash is pure shit. One can't remove a value from array (with unset),
            # so, let's check instead on each iteration that it is not the unwanted value.
            # Seriously, fuck bash.
            if [ "$f" != "$output" ] ; then
                echo "Where's the file $f ??"
                echo "file 'video_duplicate_$f'" >> videos_to_concat.txt
                ffmpeg -i "$f" -c copy "video_duplicate_$f"  # Rewrite video, sony timestamps are broken
            fi
        done

        ffmpeg -f concat -i videos_to_concat.txt -c copy $output
        rm videos_to_concat.txt video_duplicate_*
    fi
}

# Usage: 'flat firefox' calls 'flatpak run org.mozilla.firefox' 
function flat {
    matches=$(flatpak list | grep -iw $1) # case insensitive, whole words
    if [ "$matches" = "" ]; then echo "Program not found."
    elif [ $(wc -l <<< "$matches") -gt 1 ]; then echo "Mutiple matches."
    else flatpak run $(cut -f 2 <<< "$matches")
    fi
}

# Print lines of code. Usage: loc '*.cc'
function loc {
    find . -name $1 -print0 | wc --files0-from=- -l
}

alias cd..="cd .."

# Find text recursively. For examble: eti some_text
alias eti="grep -rniIF -B 1 -A 1 --exclude-dir=target"
# Find text recursively within current directory using regular expression. For examble: eti some\Wtext
alias etir="grep -rniIE -B 1 -A 1 --exclude-dir=target"

# Package manager shortcuts
alias sagi='sudo apt-get install'
alias ap='aptitude'
# Update system
alias saguu="sudo apt-get update && sudo apt-get dist-upgrade -y && flatpak update -y"

# Fix broken audio
alias killaudio="pulseaudio -k && sudo alsa force-reload"
# Print most recently modified files in current directory
alias vikat="find -type f | xargs -d '\n' stat --format '%Y :%y: \033[1;30m%n\033[0m' 2>/dev/null | sort -nr | head | cut -d: -f2,3,5 | xargs -0 echo -e"
# Calculate checksum for current directory INCLUDING filenames and permissions
alias dirsum1="tar c . | md5sum"
# Calculate checksum for current directory NOT INCLUDING filenames and permissions
alias dirsum2="find . -type f -name '*' -exec md5sum {} + | awk '{print $1}' | sort | md5sum"
# Scan wifi networks. This also refreshes the wifi-list, so if device was not connected, it makes auto-connection quicker
alias wifi="sudo iwlist scan | grep 'Cell\|ESSID:\|Quality='"
# Test that internet is working by pinging google
alias pingu="ping -c 1 google.com"
# Show my ip addresses
alias myip="ip addr show dev $(ip -o -4 route show to default | awk '{print $5}'| head -n1) scope global | grep inet | tr -s ' ' | cut -d ' ' -f 3"
# Give user permissions for all files the folder
alias VITUNPASKASAATANA="sudo chown -R $USER:$USER ."

# SSH to csc
alias etatyo="ssh tolvane2@puhti-login1.csc.fi"
# Mount ssh filesystem to csc
alias yhista="sshfs tolvane2@puhti-login1.csc.fi:/ /home/alpi/PC/laskut/kesä_2020/csc_NOSYNC/juuri"
alias erota="fusermount -u /home/alpi/PC/laskut/kesä_2020/csc_NOSYNC/juuri"

# Change screen for wacom tablet
alias wacsc="xsetwacom set \"Wacom Intuos BT M Pen stylus\" MapToOutput next"
# Print most recently modified files in current directory
alias vikat_E="find . -type f -name energy.h5 | xargs -d '\n' stat --format '%n   %y' 2>/dev/null | sort -nr | cut -d: -f1,2"
# Prevent myself from disabling my time-management blocks
alias crontab="echo nope"
# Test if the screen is stuck or the whole system is stuck by playing some audio
alias hello="play -n -c1 synth 3 whitenoise band -n 500 20 fade h 1 3 1"
# Delete rust target rust folders from last 30 days
alias sweepsweepsweep="cargo sweep -r -t 30 ."
# Print names of current zombie processes and their pids.
alias zombies='ps -A -ostat,ppid | awk '"'"'/[zZ]/ && !a[$2]++ {print $2}'"'"' | xargs -I {} ps -p{} -o '"'"'comm= pid='"'"
# Git log graph
alias glg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)' --all"
# Git gui
alias gtup="flatpak run com.github.Murmele.Gittyup"
# Copy and paste from clipboard (sudo apt-get install xclip)
alias "cx=xclip -selection clipboard"
alias "vx=xclip -o -selection clipboard"


# Helper for myself: create symbolic link to home folder root
# cd ~
# mv .zshrc .zshrc_old
# ln -s ./Documents/Tietsikka/asetukset_ja_configuraatiot/linux_utility_scripts/zshrc .zshrc
