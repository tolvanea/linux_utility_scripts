#!/usr/bin/python3

import subprocess
from subprocess import STDOUT, PIPE, CalledProcessError
import sys
import os
import re

help_text = """

Turn fimfic text files to audiobooks with text-to-speech.

This script does
    - strip out charactes like ">", so that TTS does not try to pronouce them "greater than"
    - Divide full book to chapters
    - output .wav files for the chapters
    - convert .wav files to modern opus audio format (requires ffmpeg)


Installation (On ubuntu):
    pip install TTS
    sudo apt-get install ffmpeg  # For converting .wav file to .opus file
    sudo apt-get install espeak  # For backup if the other TTS fails

Note:
    GPU-support makes it Text-to-speech conversion faster, but requires installation of
    CUDA drivers, which is pain in the ass. If you manage to install those cursed drivers,
    go ahead and enable the GPU support. You need to
        1. pip install onnxruntime-gpu
        2. Removing comment, that is the "#" character on line
            #"--use_cuda", "1",
            below.

    If you run on windows, on which installation of programs is difficult, and you dont want
    to install ffmpef, then you need not to convert the .wav file to compressed .opus. For that
    you need just to uncomment the lines 'subprocess.run(["ffmpeg",....])' below.

Usage:
    1. Download fimfic as .txt-file and place it into current folder.
    2. Run
        fimfic_to_audiobook.py "Some awesome story.txt"
    3. New folder is generated with each chapter as a audio file. Enjoy.

Usage: (Onwards from chapter 10:)
    fimfic_to_audiobook.py "Some awesome story.txt" --continue=10

Usage: (Only chapters 10,11, and 21:)
    fimfic_to_audiobook.py "Some awesome story.txt" --chapters=10,11,21



See more about TTS: https://github.com/coqui-ai/TTS
"""

# If you try to read this source code, it may be better to start from bottom,
# as main function and "chapters_to_speech" is down there.

# Takes in book filename (.txt) that is downloaded from fimfiction
# Returns list<list<string>>, where outer list is for chapters, and inner list is for lines.
def get_chapters(filename):
    chapter_separator = "> --------------------------------------------------------------------------"

    with open(filename) as fp:
        lines = fp.readlines()

    # First lines of book goes like this:
    #title = lines[0][1:]
    #author = lines[1][1:]

    prev_line = lines[0][2:]
    chapters = [[prev_line]]  # First entry here is a title, and not actual chapter

    for next_line0 in lines[1:]:
        next_line = next_line0.replace(" ", " ")  # Remove special spaces (no brake space)
        next_line = next_line.replace("—", "-")  # Remove special dashes
        next_line = next_line.replace("–", "-")  # Remove special dashes
        next_line = next_line.replace("_", "")   # Remove underscores
        next_line = next_line.replace("…", "...")
        next_line = next_line.replace("...", ".")
        next_line = next_line.replace("“", '"')
        next_line = next_line.replace("”", '"')
        next_line = next_line.replace("’", "'")
        next_line = next_line.replace("‘", "'")
        next_line = next_line.replace("/", "")
        next_line = next_line.replace("\\", "")
        next_line = next_line.replace("&", "and")
        next_line = next_line.replace("*", "")
        next_line = next_line.replace("∗", "")
        next_line = next_line.replace("`", "")
        next_line = next_line.replace("∵", "") # This character was in "Hard Reset 2 Reset Harder"
        # Drop down chapter title ">" and separator "> -----..."
        if (prev_line.startswith("> ")) and (next_line.startswith(chapter_separator)):
            chapter_title = chapters[-1].pop()
            next_chapter = [
                chapter_title[2:].strip()+"\n",  # Drop ">"-character out from line with chapter name
                "\n",                        # Remove chapter separator "> ---------..."
            ]
            chapters.append(next_chapter)
        else:
            chapters[-1].append(next_line)
        prev_line = next_line

    if len(chapters) < 2:
        return chapters  # No fimfic-style chapters found, returning all text in one chapter
    else:
        return chapters[2:]  # Skip title in first chapter so that the chapters starts with its name

# Some chapters have numbers like III. Voice reads it "ai ai ai"
# Also, if there is an arabic number like 3. there, remove it and replace with
# actual number that is counted from number of chapters so far.
def get_rid_of_roman_numbers(chapter_title, chapter_number):
    words = chapter_title.split()
    first_word = words[0].lower().replace(".", "")

    roman_number = True
    for char in first_word.upper():
        if char not in ["I", "V", "X", "L", "C"]:
            roman_number = False

    has_number = first_word.isdigit()

    if roman_number:
        first_word = str(chapter_number) + "." # Replace roman number with actual number
        has_number = True
    elif has_number:
        first_word = str(chapter_number) + "." # Make sure that there's dot after chapter number.
    else:
        return chapter_title, has_number  # did not detect any number, return original title line

    return " ".join([first_word] + words[1:]), has_number

# Coverts .wav file generated by TTS to .opus file, which takes much less space.
# Opus is better format than mp3 or AAC.
def wav_to_opus(chapter_name, wav_file):
    # Compress to opus format, and remove high pitch noise above 9000kHz
    finished_call = finished_call = subprocess.run([
        "ffmpeg",
        "-i", wav_file,
        "-filter:a", "lowpass=f=9000",
        chapter_name + ".opus",
    ], capture_output=True, check=True)
    os.remove(wav_file)

# This is the beef.
# Read the chapter (or part of chapter) with AI based TTS
def make_the_audio_file_from_text(chapter_name, text):
    wav_file = chapter_name + ".wav"
    finished_call = subprocess.run([
        "tts",
        "--use_cuda", "1",
        #"--model_name", "tts_models/en/ljspeech/glow-tts",
        #"--model_name", "tts_models/en/ljspeech/tacotron2-DDC",
        "--model_name", "tts_models/en/ljspeech/vits",
        "--text", text,
        "--out_path", wav_file,
    ], capture_output=True, check=True)
    wav_to_opus(chapter_name, wav_file)

# If above AI based TTS fails, use this much more crappyer TTS that
# sounds like robot in a tin can. Usually only few senteces need to
# be read with it.
# I consider it as a bug of this script if this backup function needs
# to be used.
# The AI-based TTS crashes A LOT, and majority of my script is only for
# trying to go circumvent those text patterns that crash it.
def TTS_fallback(chapter_name, text):
    wav_file = chapter_name + ".wav"
    finished_call = subprocess.run([
        "espeak",
        "-s", "150", "-p", "110",
        text,
        "-w", wav_file,
    ], capture_output=True, check=True)
    wav_to_opus(chapter_name, wav_file)

# Drop some more characters patterns that seem to crahs the TTS.
# This is done before splitting the text in chapters.
def drop_characters_that_crash_tts(text):
    # some characters (' and -)  at wrong places tend to crash TTS
    text = re.sub(r'-{2,}', '', text)  # Remove dash-walls "---------"
    return text\
        .replace(" -", " ").replace("- ", " ")\
        .replace("\n.", "\n").replace(" . ", " ")\
        .replace("\n-\n", "\n")\
        .replace("<", "").replace(">", "")
        #.replace("'\n", "\n").replace("\n'", "\n")\
        #.replace("-\n", "\n").replace("\n-", "\n")\

# Ok, so, when TTS crashes reading a chapter, try dividing the chapter in
# half, and process those halfs separately. When the other chapter of the two crashes,
# divide it to half and continue so on. At the end there is left only few sentences,
# which is short enough that it does not make much harm to drop characters like " and '
# out of it. Usually that fixes the crash. If it does not, call backup TTS. Then concatenate
# all the chapter pieces to one chapter again, and delete the pieces. If you see some
# chapters named like a b b a b a, that notates the splits of former (a) and latter (b) parts.
def recursive_splitting(chapter_name, text):
    # Fix common characters that happen to crash TTS.
    if len(text) < 1000: # Input is short for later stages of recursive splitting
        if text[0] in ['"', "'"]:
            text = text[1:]
        if text[-1]  in ['"', "'"]:
            text = text[:-1]
        if text.count('"') in [1, 3, 7, 9, 11]:
            text = text.replace('"', "") # Odd " crashes TTS
        text = text.replace(' "', " ").replace('" ', " ")\
            .replace('\n"', "\n").replace('"\n', "\n")\
            .replace(" '", " ").replace("' ", " ")\
            .replace("\n'", "\n").replace("'\n", "\n")
    try:
        print("Processin chapter", chapter_name)
        make_the_audio_file_from_text(chapter_name, text)
        return [chapter_name], True
    except CalledProcessError as err:
        print("Oh, TTS crashed, splitting the text in half and doing the parts 'a' and 'b' separately")
        # Try if splitting the text in two on sentence end.
        half_way = text.find(".", len(text)//2) + 2
        if half_way > 5 and half_way < len(text)-5: # Could be split in half
            text_part_1 = text[:half_way]
            text_part_2 = text[half_way:]
            audio_chapters1, success1 = recursive_splitting(chapter_name + " a", text_part_1)
            audio_chapters2, success2 = recursive_splitting(chapter_name + " b", text_part_2)
            with open(os.path.dirname(chapter_name) + "/debug.txt", "a") as f:
                for ch1 in audio_chapters1:
                    f.write("    " + ch1 + "\n")
                f.write("--" + "\n")
                for ch2 in audio_chapters2:
                    f.write("    " + ch2 + "\n")
                f.write("----" + "\n")
            return audio_chapters1 + audio_chapters2, success1 and success2
        else: # Fuck. Can not split anymore, doing it with shitty robotic-ass TTS then
            msg = "Can not split anymore.\n" +\
                "The following text could not be processed with AI based TTS, " +\
                "and is read with a crappy and robotic TTS voice:\n\n" + text +\
                "\n\n\nstderr:\n\n" + str(err.stderr) + "\n\nstdout\n" + str(err.stdout) + "\n"
            print(msg)
            with open(chapter_name + " TTS failure" + ".txt", "w") as f:
                f.write(msg)
            TTS_fallback(chapter_name, text)
            return [chapter_name], True
    except Exception as e:
        err_msg = "\n\n\nSHOULD NOT HAPPEN. FAIL FAIL FAIL FAIL\n" + str(e) + "\n\n\n"
        with open(chapter_name + "INTERNAL FAILURE" + ".txt", "w") as f:
            f.write(err_msg)
        print(err_msg)
        return [], False

# Combines multiple audio files into one audio file.
# Used to construct chapter from multiple pieces
def combine_audio_files(audio_files, output_name):
    combination_list = (output_name + " combination" + ".txt")
    with open(combination_list, "w") as f:
        for audio in audio_files:
            f.write("file '" + os.path.basename(audio) + ".opus'\n")
    finished_call = subprocess.run([
        "ffmpeg",
        "-f", "concat", "-safe", "0",
        "-i", combination_list,
        "-c", "copy", output_name + ".opus"
    ], capture_output=True, check=True)
    os.remove(combination_list)
    for audio in audio_files:
        os.remove(audio + ".opus")

# Main loop, call TTS for each chapter individually, and save audio file for each shapter.
def chapters_to_speech(chapters, fic_name, start_chapter, only_chapters):
    os.makedirs(fic_name, exist_ok=True)

    for i, chapter in enumerate(chapters):
        # skip chapters if the "--continue=" or "--chapters=" parameter is given
        if (start_chapter is not None and i < start_chapter - 1) or\
           (only_chapters is not None and i+1 not in only_chapters):
            continue

        if len(chapters) > 1:
            chapter_title, has_number = get_rid_of_roman_numbers(chapter[0].strip(), i+1)
        else:
            chapter_title, has_number = fic_name, False
        chapter_title = chapter_title.replace("'", "’") # ffmpeg doesn't like ':s in a filename
        chapter_title = chapter_title.replace(":", "")  # some filesystems don't like ':'s in a filename

        if not has_number:
            chapter_base = "{} {}".format(i+1, chapter_title)
        else:
            chapter_base = "{}".format(chapter_title)

        chapter_name = fic_name + "/" + chapter_base
        text = "".join([chapter_title] + chapter[1:])
        text = drop_characters_that_crash_tts(text)

        try:
            audio_files, success = recursive_splitting(chapter_name, text)
            if len(audio_files) > 1:
                if not success:
                    chapter_name = chapter_name + " MISSING PARTS"
                combine_audio_files(audio_files, chapter_name)
        except CalledProcessError as err:
            msg = (
                "\n\n\nFuuuuq. Unknown error on '" + chapter_name + "'\n" +
                "Skipping the chapter. This went wrong:"  +
                "\n\ncommand:\n" +
                str(err.cmd) +
                "\n\nSTDOUT:\n" +
                str(err.stdout) +
                "\n\nSTDERR:\n" +
                str(err.stderr)
            )
            print(msg + "\n\n\n")

            with open(fic_name + "/errors.txt", "w") as f:
                f.write(msg)
    print("All chapters done!")

# Parse arguments, read .txt file to memory, and then call TTS for it.
def main():
    args = sys.argv
    if len(args) < 1 or not args[1].endswith(".txt"):
        print("Give one argument, which is path to the full .txt file downloaded from fimfic.\n")
        print(help_text)
        return

    elif args[1] in ["--help", "-h,", "help", "halp"]:
        print(help_text)
        return

    start_chapter = None
    only_chapters = None
    if len(args) > 2:
        if args[2].startswith("--continue="):
            start_chapter = int(args[2][11:])
        elif args[2].startswith("--chapters="):
            only_chapters = [int(ch) for ch in args[2][11:].split(",")]
        else:
            print("Unknown parameter", args[2])
            print("For example:")
            print("    - To continue from chapter 5 onwards, it should be: --continue=6")
            print("    - To process only chapters 5,6 and 9, it should be: --chapters=6,7,10")
            return
    if len(args) > 3:
        print("Too many arguments (3 or more)!")
        return

    filename = args[1]
    fic_name = os.path.splitext(os.path.basename(filename))[0].replace("'", "’")

    chapters = get_chapters(filename)
    chapters_to_speech(chapters, fic_name, start_chapter, only_chapters)

main()
