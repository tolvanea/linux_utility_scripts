#!/bin/bash 
# This script copies files from remote ssh server to local folder, and then copies local files
# back, synchronizing folders.
#
# If files are removed on home pc, they will be removed on csc. If files are removed on csc, they
# will not be removed on you home pc.
#
# Note, to not type your password, please add content of your home pc:s
#     ~/.ssh/id_rsa.pub
# into ssh-server's file
#     ~/.ssh/authorized_keys
#
# Usage:
#     rsync_calculations.sh

pc='/home/alpi/PC/laskut/2023'
csc='tolvane2@puhti.csc.fi:/scratch/trantal1/tolvanea'
exclude_flags='--exclude target --exclude venv --exclude *NOSYNC*'

if [ ! -d $pc_calc ]; then
    echo "Missing mount point $pc_calc"
    exit 1
fi


# IMPORTANT! The slash "/" in the end of path is mandatory here!
# Also, all directories must exits.
# To test what would be copied, use flag "--dry-run".
rsync -avuz --delete $exclude_flags "$csc/to_pc/" "$pc/from_csc/"
rsync -avuz --delete $exclude_flags "$pc/to_csc/" "$csc/from_pc/"


# rsync flags above:
#   -a, --archive               archive mode; equals -rlptgoD. Made for backups.
#   -v, --verbose               increase verbosity, print all files that are (or would be) copied
#   -u, --update                skip files that are newer on the receiver
#   -z, --compress              compress file data during the transfer
#   -n, --dry-run               perform a trial run with no changes made
#       --delete                delete extraneous files from dest dirs
#       --exclude=PATTERN       exclude files matching PATTERN
