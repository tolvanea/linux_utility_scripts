"""
This is small proram that pings a website regurlarly, and plots real time graph from it.
Two different pings are called: One with Mullvad VPN tunneling and one without. The both
are plotted. (This program can be used also without Mullvad vpn, but it's not very elegant anymore)

Word of caution: This application steals window focus. It is also hard to terminate, and does
not really close from red cross of top window bar. So it must be closed with 'ctrl+c' in terminal.
It may also responds amazingly poorly to raw 'ctrl+c'. This all is due to shitty matplotlib
windowing, not due to bad coding here, I promise! 😝

Usage:
  python3 ping_monitor.py

There are few of parameters that can be tweaked in source code below
  * host:     Host that is pinged
  * timeout:  Number of second to wait for ping command to responds
  * packets:  Number of packets that is pinged with one 'ping' command
  * outfile:  Optional outputfile in which data is stored in csv format
  * mullvad:  Test with or without bypassing mullvad vpn.

Dependency installation instructions:
  sudo apt-get install python3-pip
  pip3 install matplotlib numpy

Alpi Tolvanen, Licence: MIT
"""
# This is host that is pinged. It can be ip or domain. For example 'google.com'
host = '157.24.2.14'  # www.lut.fi

# 'timeout' is the time waited before ping command is dropped
# Also, if ping has this value in graph, it means that the packet was never received
timeout = 1.000 # timeout for ping in SECONDS.

# Sleep this many seconds between pings.
sleep_time = 1.0

# 'packets' determines the number of packets that are pinged with one command. Changing it to >1 is not
# really recommended as it makes graph slowly responding and harder to close by ctrl+c.
packets = 1

# Output file. If given, then ping results are saved to it in csv format.
# Could be for example "ping_log.csv"
outfile = None

# Logarithmic plot
log_plot = True

# Compare difference with and without mullvad vpn. This program is made mullvad in mind, so if
# that is disabled, then two lines are still plotted, but even though they are the same thing!
mullvad = True

#################### Tweakable parameters ends here ###################

import re
import subprocess
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt # Run pip3 install matplotlib
import numpy as np # Run pip3 install numpy
import collections

assert(packets - 1 < timeout) # Note that ping commands takes at least 'packets - 1' seconds!

if mullvad:
    commands = [
        ['mullvad-exclude', 'ping', '-c', str(packets), '-q', host],
        ['ping', '-c', str(packets), '-q', host],
    ]
    labels = ["Mullvad bypass", "Mullvad on"]
    colors = ["r", "c"]
else:
    commands = [['ping', '-c', str(packets), '-q', host]]
    labels = ["ping"]
    colors = ["r"]


def live_plotter_init_axis(title=None, x_label=None, y_label=None):
    plt.style.use('ggplot')
    plt.ion()
    fig, ax = plt.subplots(1, 1, figsize=(13,6))
    if title is not None:
        ax.set_title(title)
    if x_label is not None:
        ax.set_xlabel(x_label)
    if y_label is not None:
        ax.set_ylabel(y_label)
    plt.show()
    fig.tight_layout()
    fig.subplots_adjust(left=0.09)
    return ax

def loop():
    ax = live_plotter_init_axis(y_label="ping (ms)")
    x_vecs = []
    y_vecs = []
    lines = []

    for i in range(len(labels)):
        size = 1000
        # Use circular buffers instead of vectors so there is no need to reallocate on every frame
        x_vec = collections.deque(maxlen=size)
        y_vec = collections.deque(maxlen=size)

        now = datetime.now()
        for j in range(size):
            x_vec.append(now + timedelta(seconds=(-size+j)*sleep_time))
            y_vec.append(np.nan)

        line, = ax.plot_date(x_vec, y_vec, colors[i], alpha=0.8, label=labels[i])
        x_vecs.append(x_vec)
        y_vecs.append(y_vec)
        lines.append(line)

    ax.legend()
    if log_plot:
        ax.set_yscale('log')

    while True:
        min_y = 999999.0
        max_y = -999999.0
        std_y = -999999.0
        for i in range(len(lines)):
            try:
                out = subprocess.check_output(commands[i], timeout=timeout).decode('utf8')
                received = re.search(r'(\d+) received', out).group(1)
                packet_loss = packets - int(received)
                statistic = re.search(r'(\d+\.\d+/){3}\d+\.\d+', out).group(0)
                parts = re.findall(r'\d+\.\d+', statistic)
                time_max = float(parts[2])
            except subprocess.TimeoutExpired:
                time_max = timeout * 1000
                packet_loss = 1
            except:
                time_max = np.nan
                packet_loss = np.nan

            now = datetime.now().replace(microsecond=0).isoformat()
            printline = "{}, {}, {}, {}".format(i, now, packet_loss, time_max)
            if outfile is not None:
                with open(outfile, "a") as f:
                    f.write(printline + "\n")
            print(printline)

            y_vecs[i].popleft()
            y_vecs[i].append(time_max)
            x_vecs[i].popleft()
            x_vecs[i].append(datetime.now())

            lines[i].set_data(x_vecs[i], y_vecs[i])

            min_y = min(np.nanmin(y_vecs[i]), min_y)
            max_y = max(np.nanmax(y_vecs[i]), max_y)
            std_y = max(np.nanstd(y_vecs[i]), std_y)
            if np.isnan(min_y) or np.isnan(max_y) or np.isnan(std_y):
                min_y = max_y = std_y = 1.0;
        ax.set_ylim([min_y, max_y + std_y])
        ax.set_xlim([x_vecs[0][0], x_vecs[0][-1]])
        plt.pause(sleep_time)


def main():
    if mullvad:
        description = "'1' means mullvad on"
    else:
        description = "0 means nothing"
    printline = "{},    {},    {} {},    {}".format(
        description,
        "system time",
        "packets lost from total of",
        packets,
        "ping time in ms"
    )
    if outfile is not None:
        with open(outfile, "a") as f:
            f.write(printline + "\n")
    print(printline)

    loop()

main()
