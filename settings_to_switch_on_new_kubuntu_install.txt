# This is my personal file, meant for listing all the settings to switch on fresh
# Kubuntu install. Do not expect to understand what most of settings mean.

# KAIKKI UUDEN SYSTEEMIN ASENNUKSESSA TEHTÄVÄT JUTUT 


Perus asennukset
#######################
    Asenna syncthing
        sudo apt-get update
        sudo apt-get install syncthing
    autostart -> “Start Syncthing”

    Muut yleishyödylliset ohjelmat:
        sudo apt-get install git git-gui aptitude htop curl kompare zsh build-essential cmake

    Global theme: breeze dark (aseta nyt ettei se resetoi muutoksia myöhemmin!)


Terminal Konsole
############################################
    System settings -> Custom shortcuts -> examples:
        ctrl+alt+T

    Modigy shortcuts
        copy        ctrl + C
        paste       ctrl + V
        new tab     ctrl + T

    Oh-my-zsh
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    Sit kun synchingi tehny tehtävänsä:
        rm .zshrc
        ln -s /home/alpi/Documents/Tietsikka/asetukset_ja_configuraatiot/tietsikan_asetukset/zshrc .zshrc

    cloonaa kansioon .oh-my-zsh/custom/plugins
    git clone https://github.com/zsh-users/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting
    
    Settings
        General: [poista] show menubar ctrl + shift + M
        
        Profiles
            Uusi Profile "white on blue"
                General
                    Initial terminal size: 110 : 30
                Appearance:
                    Muokkaa "white on black"
                        background color (eka loota) HTML-koodi #003030
                            hue = 180
                            sat = 255
                            Val = 48
                        transparency : 4%
                        Font: Hack 13
                    Cursor -> blinking
                Scrolling: 10000 lines
            "set as default"
    
        TabBar: close tab on middle click

    # TODO deprecate this!
        # Toimiikohan tää komento noin?
        cargo install tealdeer bottom tokei

    stderred
        git clone https://github.com/sickill/stderred.git
        cd stderred
        make
        ## Loput ovat zshrc:ssä


Dolphin-tiedostoselain
#######################
    asetukset
        General (side-tab) 
            Behavior (tab) -> 
                [poista] Show selection marker  (kansioiden vihreät pallukat pois)
                //[ruksi] Remember properties for each folder // Deprecate?
            Status Bar (tab) -> 
                [poista] Show zoom slider
                [poista] Show space information  (turhaa krääsää pois näytöltä)
        shortcuts
            Back:   Alt + down
                


# Näppis kuntoon starttiskriptillä! (Ellei jo tehty custom firmiksessä)
####################################

Mene ~/PC/ohjelmat/keyd ja suorita
    git pull
tai sitten
    cd ~/PC/ohjelmat/keyd
    git clone https://github.com/rvaiya/keyd
    cd keyd

Suorita
    git checkout $(git describe --tags --abbrev=0) # viimeisin tagi
    make && sudo make install

sudo systemctl enable keyd && sudo systemctl start keyd

sudo nano /etc/keyd/default.conf

copy-pasteta KAIKKI TEKSTI riville "&& sudo systemctl restart keyd ..." saakka!

echo "[ids]

*

[main]
capslock = layer(shift)
insert = layer(shift)
pageup = home
pagedown = end
leftmeta = layer(altgr)
sysrq = layer(meta)
" \
| sudo tee /etc/keyd/default.conf \
&& sudo systemctl restart keyd

# Printtaa system servicet: systemctl --no-pager
# Printtaa keyd:n logit: sudo journalctl -e -u keyd.service



# KDE asetukset
#################
    
    # Työpöydän säädöt
    Virtual desktops
        5 riviä
        15 kpl 
        [vaiha toinen tabi] - No animation
        
    #Global Keyboard Shortcuts --> Kwin
        switch one desktop up/down/left/right  --  ctrl + alt + nuolet
        window one desktop up/down/left/right  --  ctrl + shift + alt + nuolet
        //show desktop -- super+D
        maximize / minimize window -- alt + home / end  (soveltuen thinkpadille)
        
    
    [right click desktop] -> configure desktop 
        -> Mouse actions -> desktop scrolling wheel off, middle click paste off
        
        
    # Ikkunan hallinta -> 
        window behavior -> [tab: titlebar actions]
            middle button lower
        Task Switcher ->
            Change walk through all windows reverse -- alt + shift + tab
            walk through windows in current application -- Alt + § / Alt + ½
        
    # screen edges
        poista vasemman ylänurkan erikoistoiminto
        
    # task manager, eli tehtäväpalkki jossa on avoimien sovellusten-ikonit:
        middle click: new instance;
        show only from curren screen -- on;
        cycle through task with mouse wheel -- off;

    # vahnat sessiot eivät avaudu uudelleenkäynnistäessä
        // Hetkinen miksi tää?
            system settings - startup and shutdown - desktop sessions - restore manually save session
        
    # Configure clipboard (sivupalkin kautta, ei asetusvalikosta)
        Shortcuts
            Show items at Mouse Position -- ctrl alt z
            
    # clock
        show week numbers
        
    # [Klikkaa wifi-ikonin alapuolisesta nuolesta] Configure system tray
        Vaults disabled
        
    # Energy saving
        when laptopn lid is closed SLEEP even when external monitor is connected

    # Kwin scripts
    % Khronkite
        Asenna se
        Valitettavasti ei tule asennusvalikkoa näkyviin ilman:
            https://github.com/esjeon/krohnkite

        - Eka tabi
            - Tile, monocle, quarter layouts
        - vika tabi
            - remove bordrers
        Golbal kwin shortcuts
            Down            : alt + -
            Up              : alt +
            Move up         : alt + shift + .
            Move down       : alt + shift + -
            set master      : alt + enter
            Float           : meta + F
            Float all       : meta + shift + F
            Monocle layout  : meta + M
            Quarter layout  : meta + q
            Tile layout     : meta + T

    Window decorations
        shadows: Very large

    Notifications:
        Hide after 2s

snap vittuun
    Poista jokanen ohjelma käsin eka snapin kautta, ja poista snappi vasta sitten. Muuten ne jäävät ikuisiksi ajoiksi koneeseen kiinni



Firefox
#####################
    Kirjaudu sisään
    
    Yläpalkki:
        [<-] [->] [osoite] [refresh] [haku] [dowloads] [bookmarks] [history] [device tabs] [valikko]
    
    about:config
        # Jotta tree style tabin chrome.css coi muokata:
        toolkit.legacyUserProfileCustomizations.stylesheets to true
        
    Lisäosa-asetukset
        Tree Style Tab
            Ylävälilehdet piiloon
                (https://github.com/piroor/treestyletab/wiki/Code-snippets-for-custom-style-rules#for-userchromecss)
                
                JOKO
                    cd /home/alpi/.mozilla/firefox/td0tofjc.default-release/
                tai flatpakin tapauksessa
                    cd ~/.var/app/org.mozilla.firefox/.mozilla/firefox/XXXXX.default-release/


                mkdir chrome
                kate chrome/userChrome.css
                
                    /* Hide horizontal tabs at the top of the window */
                    #main-window[tabsintitlebar="true"]:not([extradragspace="true"]) #TabsToolbar > .toolbar-items {
                        opacity: 0;
                        pointer-events: none;
                    }
                    #main-window:not([tabsintitlebar="true"]) #TabsToolbar {
                        visibility: collapse !important;
                    }

                    /* Hide TST title on side bar  */
                    #sidebar-box[sidebarcommand="treestyletab_piro_sakura_ne_jp-sidebar-action"] #sidebar-header {
                        display: none;
                    }
                
            customize toolbar -> alhaalta disabloi "show titlebar" VOI VAATIA ON-OFF SYKLIN!
                
    Settings
        Privacy & Security 
            Use a primary/master password
            -> Firefox Data Collection and Use
        Search -> duckduckgo

        
Kate säädöt
#################################
    settings (pudotusvalikko) -> [poista] show statusbar (lisää tilaa näytölle)
    settings (pudotusvalikko) -> [poista] show tabs      (lisää tilaa näytölle, tabit saa vasemmalta")
    settings (pudotusvalikko) -> [poista] show menubar   (lisää tilaa näytölle)

    plugins
        Ota pois terminal ja filesystem

    settings -> appearance

    settings -> editing
        Uncheck: Move selected text if dragged
        copy cut current line if no text selected
        show statick word wrap marker : 100
        Disable Automatially close bracket
        animate matching bracket
    
    shortcuts
        search in files - ctrl+shift+F
        quit          - ctrl + shift + Q    (ei vaarallisia ctrl+s misklikkejä)
        close file    - ctrl + shift + w    (ei vaarallisia ctrl+s misklikkejä)
        block edit    - ctrl + b            
        duplicate line down             ctrl + D
            Delete original duplication shortcuts, which overlap with ctrl+alt+arrows
        Move cursor to next mathching ident - ctrl+up
        save all        - ctrl s
        save            - ctrl + shift + s
        delete line     - ctrl + u
        show documents  - f1
        go to next/prev editing location    - alt + left / right
        
# Wacom
    Laitteen koodi pysyy samana, mut voit tarkistaa sen tällä:
        lsusb

    sudo bash -c "echo 'SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"056a\", ATTRS{idProduct}==\"0378\", ENV{DISPLAY}=\":0\", ENV{XAUTHORITY}=\"/home/alpi/.Xauthority\", RUN+=\"/home/alpi/Documents/Tietsikka/linux_utility_scripts/wacom_intuos\"' > /etc/udev/rules.d/99-wacom_tablet.rules"
    
# Crontab
    Editoi:
        sudo crontab -e

    Sitten laita sinne

# TODO, fill:
MACHINE=pahvilaatikko
COMMAND=rsync_varatikku.sh

CONFS="/home/alpi/Documents/Tietsikka/asetukset_ja_configuraatiot/tietsikan_asetukset"
LOG="$CONFS/$MACHINE/cron.log"

CMD="/home/alpi/Documents/Tietsikka/scriptit_pathissa/$COMMAND"
SHELL=/bin/zsh
PATH="/bin:/usr/bin"

# BEWARE, CRONTAB IS FUCKING STUPID:
#  - Variable susbstitution does not work in cron, so do not write PATH="$PATH:/..."
#  - Crontab thinks that a '%' in a command entry means newline. Escape it whith \%
#  - Also some more complex commands do not work for unknown reason.
#  --> Use a single line bash script to avoid this shit

1 12,17,22 * * * /usr/bin/zsh -c "{/bin/date; echo '\n'; $CMD;} > $LOG"
#* * * * * /usr/bin/zsh -c "{/bin/date; echo '\n'; $CMD;} > $LOG"
# Fix jbl speakers
* * * * * su -c 'play -n -c1 synth 60 whitenoise band -n 1 1 fade h 1 60 1 gain -10000' alpi




# Asennukset:
    sudo apt-get install python3-pip chromium-browser gimp
    pip3 install numpy scipy matplotlib sympy

    Discoverin kautta flatpakeja:
        signal
        telegram
        keepassxc
        
    Zotero
        Edit->pfererences->advanced->Files and folders->Työ/akatemia/zotero
    
    rust
        sudo apt install build-essential pkg-config libssl-dev
        curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
        cargo install cargo-edit cargo-sweep

    vscodium
        rust-analyzer, better-toml, git-lens

        add shortcuts
            copy line down:     ctrl + d
            go forward:         shift + alt + right
            go back:            shift + alt + left
            help: doc.          ctrl + q
            move line up        ctrl + shift+ up
            move line down      ctrl + shift+ down
            replace:            ctrl + R
            go to definition    ctrl + alt + d
            go to implementa.   ctrl + alt + e
            Save all files      ctrl + s
        REMOVE OVERLAPS
        delete shortcuts
            close window:       ctrl + w
        leave shortcuts
            add cursor above/below: ctrl +


        never close braket:
            auto closing bracket    never
            Auto closing overtype   never
            auto surround           never
        margin indicator: rulers to 100 in json
        font size 16
        yarra valley
        braket pair colorizer


        


    
    
