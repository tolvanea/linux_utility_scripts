#! /usr/bin/python3
# Prints how much I have spent/received money from different recipients/senders
# Works on csv files of Osuus Pankki
#
# Assuming that you have files from "tiliote_2016.csv" to "tiliote_2017.csv" in the
# same folder as this script, run it with command:
#     python3 summarize_op_csv.py tiliote_2016.csv tiliote_2017.csv
#
# Or if you have modified this source code to iterate through all your CSVs
#     python3 summarize_op_csv.py --yea_i_modified_it_in_source_code
import csv
import sys
import os

usage = """
Summarizes where you have spent most money using csv file imported from OP-bank.

Usage example:
    summarize_op_csv.py tiliote_2022.csv tiliote_2023.csv
"""

def total_by_service(filename):
    sums = {}
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)  # Skip the first line

        for row in csv_reader:
            service = row[5]
            if service == "":
                service = row[4]  # The name is empty for cash transfers

            value = float(row[2].replace(",", "."))

            if service not in sums:
                sums[service] = 0.0
            sums[service] += value

    expences = 0.0
    income = 0.0
    visited_once = False
    for service, value in sorted(sums.items(), key=lambda t: t[1]):
        if value < -100.0 or 100.0 < value:
            print("{:>44}: {:>9.2f}".format(service, value))

        if value < 0.0:
            expences += value
        else:
            income += value

        if (-100.0 < value and value < 100.0) and not visited_once:
            visited_once = True
            print(
                "{:>55}\n{:>55}\n{:>55}".format(
                    "."*36,
                    "Left out records from -100e to 100e",
                    "."*36
                )
            )
    print("{:>55}".format("."*20))
    print("{:>44}: {:>9.2f}".format("Total income", income))
    print("{:>44}: {:>9.2f}".format("Total expences", expences))
    print("\n\n")

def print_cool_title(text):
    print("-"*55)
    print("{:>28}".format(text))
    print("-"*55)

def main():
    if len(sys.argv) == 1 or sys.argv[1] == "--help":
        print(usage)
    elif sys.argv[1] == "--yea_i_modified_it_in_source_code":
        for year in ["2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023"]:
            filename = "tiliote_{}.csv".format(year)
            print_cool_title(year)
            total_by_service(filename)
    else:
        for filename in sys.argv[1:]:
            print_cool_title(filename)
            total_by_service(filename)

main()
