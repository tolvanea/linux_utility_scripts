Tämä filu sisältää ohjeet sille jos haluaa säätää wacomin käynnistymään 
automaattisesti aina kun se kytketään. (Tähän käytän udev-rules:eja.)

1. Selvitä wacom-laitteen numerokoodi
    Komento
        lsusb
    printtaa kytketyt usb-latteet. Esimerkiksi voi tulostua rivi:
        Bus 002 Device 047: ID 056a:0378 Wacom Co., Ltd 
    Missä kaksiosainen numerokoodi on 056a ja 0378.
    

1.5 Taustatietoa: Linuxissa automatisointia voi tehdä udev-rules-tiedostojen  
    avulla. Kansioon 
            /etc/udev/rules.d
    voi luoda uuden tiedoston, joka on formaattia
            SUBSYSTEMS=="usb", \
            ATTRS{idVendor}=="056a", \
            ATTRS{idProduct}=="0378", \
            ENV{DISPLAY}=":0", \
            ENV{XAUTHORITY}="/home/alpi/.Xauthority", \
            RUN+="/home/KÄYTTÄJÄ/.../WACOM_CONF"
    Kun tuo tiedosto on olemassa, aina kun kytketään (numerokoodin) 
    usb-laite suoritetaan annettu komento. Huom. muutosten aikaansaanti voi 
    vaatia uudelleenkäynnistämistä.

    
2. Korvaa seuraavaan komentohirviöön omat tiedot
    - numerokoodi 056a
    - numerokoodi 0378
    - käyttäjänimi polussa /home/KÄYTTÄJÄ/.Xauthority
    - suoritettava conffitiedosto /home/KÄYTTÄJÄ/.../WACOM_CONF
    (Katso samalla ettei tule korvattua yhtään muuta merkkiä komennossa)
    
    Yhden rivin komentohirviö joka toteuttaa kohdan 1.5:
        sudo bash -c "echo 'SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"KOODI1\", ATTRS{idProduct}==\"KOODI2\", ENV{DISPLAY}=\":0\", ENV{XAUTHORITY}=\"/home/KÄYTTÄJÄ/.Xauthority\", RUN+=\"/home/KÄYTTÄJÄ/.../WACOM_CONF\"' > /etc/udev/rules.d/99-wacom_tablet.rules"
    Johon pitää korvata KOODI1, KOODI2, KÄYTTÄJÄ ja polku skriptiin.
    Tässä on esimerkki:
        sudo bash -c "echo 'SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"056a\", ATTRS{idProduct}==\"0378\", ENV{DISPLAY}=\":0\", ENV{XAUTHORITY}=\"/home/alpi/.Xauthority\", RUN+=\"/home/alpi/Documents/Tietsikka/asetukset_ja_configuraatiot/laitteet/wacom_intuos.sh\"' > /etc/udev/rules.d/99-wacom_tablet.rules"

        
3.  Suorita muutettu komentohirviö 
    (Yhtä hyvin saman voi toki tehdä luomalla kohdan 1.5 tiedosto käsin tekstieditorilla.)

    
    
(Jos uudelleenkäynnistämisen jälkeen automaattinen skriptin suorittaminen ei 
toimikkaan, voi kaikki muutokset kumota poistamalla tiedosto 
        "/etc/udev/rules.d/99-wacom_tablet.rules" 
.)
