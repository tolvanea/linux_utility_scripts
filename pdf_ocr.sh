#!/bin/bash -e
# INSTALLATION REQUIREMENTS
# sudo apt-get install tesseract-ocr imagemagick pdftk
#
# Modified from source:
# http://tech.akom.net/archives/126-OCR-on-a-large-PDF-using-tesseract-and-pdftk.html

if [ -z "$1" ] ; then
    echo "Adds an invisible text layer to a scanned pdf. Writes to 'output.pdf'."
    echo "Usage:"
    echo "    pdf_ocr.sh your_file.pdf"
    exit 1
fi

PDF="$1"
NUM="$(pdftk $PDF dump_data output | grep -i NumberOfPages | cut -d' ' -f2)"

for PAGE in $(seq -f "%05g" 1 $NUM) ; do
    echo "Processing page $PAGE / $NUM"
    pdftk "$PDF" cat $PAGE output temp.pdf
    convert -density 300 temp.pdf -depth 8  -fill white -draw 'rectangle 10,10 20,20' -background white -flatten +matte temp.tiff
    tesseract temp.tiff tmp.pdf_"${PAGE}" pdf
    rm -f temp.tiff temp.pdf
done


pdftk tmp.pdf_*.pdf output output.pdf  && rm -f tmp.pdf_*.pdf
echo "Output written to output.pdf"
