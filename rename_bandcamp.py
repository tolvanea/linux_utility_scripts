#!/usr/bin/python3 

help_text = """
This script modifies bandcamp album song names. It removes album name and moves the song number to first. It finds all albums recursively from current directory. 

Note: Album directory name must contain album's name in it. I.e. if album's name is "AlbumName", then directory must be named somethig like "ArtistName - AlbumName" or "BlahBlahAlbumNameBlah"

For example, let's say are in your folder /home/user/Music/ . Usage is:
    python3 rename_bandcamp_recursive.py
    
This may find album "Tripon - Villains EP" in /home/user/Music/foo/bar/ . Songs
    "Tripon - Villains EP - 01 Chrysalis.m4a"
    "Tripon - Villains EP - 02 Nightmare Moon.m4a"
gets renamed to
    "01 Chrysalis.m4a"
    "02 Nightmare Moon.m4a"
    
Or you may have album "A State of Sugar - Brownie" in /home/user/Music/foo/baz/ . Songs
    "Niłch'i Poni - Brownie - 15 Skies Ablush.m4a"
    "Single Purpose - Brownie - 17 Protector Of Dreams.m4a"
gets renamed to
    "15 Niłch'i Poni - Skies Ablush.m4a"
    "17 Single Purpose - Protector Of Dreams.m4a'

    
Works only on linux / POSIX.
"""

"""
TODO Does not seem to work if hidden direcotries exists. IDK why.
"""

import os
import sys
from difflib import SequenceMatcher


if len(sys.argv) != 1:
    print(help_text)
    
# https://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings
def longestSubstringFinder(string1, string2):
    match = SequenceMatcher(None, string1, string2).find_longest_match(0, len(string1), 0, len(string2))
    m1 = string1[match.a: match.a + match.size]
    m2 = string2[match.b: match.b + match.size]
    assert(m1 == m2)
    return m1
    
    
def try_extract_album_name_from_song(song_name, directory_name):
    """
    Album name is part that is removed from song name. It does not contain dashes or leading spaces.

    Returned album name can be "artistName - albumName" if only one artist has made full album. Then that
    full string is removed from song name. Example:
    directory name is "artistName - albumName"
    song name is "artistName - albumName - 01 songName.mp3"
    stripped song name will be "01 songName.mp3"

    If album is made by multible artists, returned album name contains only the album name (and no artist)
    Example:
    directory name is "albumName"
    song name is "artistName - albumName - 01 songName.mp3"
    stripped song name will be "01 artistName - songName.mp3"
    """
    idx = song_name.find(directory_name + " ")

    # One artist has made full album, i.e. album name is now "artistName - albumName"
    # and song name is "artistName - albumName - 01 songName.mp3"
    # Yes I know album name contains artist name in this case, but I want those both removed,
    # and it works easily this way
    if idx != -1:
        return directory_name

    # Compilation album, artist is not same as album releaser
    if idx == -1:
        idx2 = directory_name.find(" - ")
        if idx2 != -1 and (len(directory_name) > idx2+4):
            directory_name_compilation = directory_name[idx2+3:]
            idx3 = song_name.find(directory_name_compilation + " ")
            if idx3 != -1:
                return directory_name_compilation
        # If above fails, the longest substring match between song name and 
        # directory name should then be the album name
        album = longestSubstringFinder(song_name, directory_name)
        if album[:3] == " - ":
            album = album[3:]
        if album[:2] == "- ":
            album = album[2:]
        if album[:1] == " ":
            album = album[1:]
        if album[-3:] == " - ":
            album = album[:-3]
        if album[-2:] == " -":
            album = album[:-2]
        if album[-1:] == " ":
            album = album[:-1]
        if len(album) < 4:
            return None
        #print("3     '{}'     ".format(album), song_name, "    ", directory_name)
        return album
        
    return None

def try_extract_album_name_from_directory(listdir, dirname):
    found_cover = False
    proposed_album_name = None
    number_of_audio_files = 0
    number_of_mathces = 0

    def is_image(name):
        return (len(name)>4) and (name[-4:] in [".png", ".jpg", "jpeg"])

    def is_music(name):
        return ((len(name) > 4) and (name[-4:] in [".m4a", ".mp3", ".flac", ".wav", ".ogg"]))

    for name in listdir:
        # Cover art
        if is_image(name) and (len(name)>6) and ("cover" in name[:5]):
            found_cover = True
            continue
        # Look for only music
        if not is_music(name):
            continue
        # For now on 'name' is a song name
        number_of_audio_files += 1
        album_name = try_extract_album_name_from_song(name, dirname)
        if album_name is not None:
            number_of_mathces += 1
            if proposed_album_name is None:
                proposed_album_name = album_name
            else:
                # It's possible that song name contains album name twice or something,
                # choose shortest common string among song names
                if len(album_name) < len(proposed_album_name):
                    proposed_album_name = album_name
                    
    if proposed_album_name is None:
        return None

    if not found_cover:
        return None

    # sanity check album name
    number_of_valid_matches = 0
    for name in listdir:
        if is_music(name) and name.find(proposed_album_name + " - ") != -1:
            number_of_valid_matches += 1

    if number_of_audio_files == number_of_mathces == number_of_valid_matches:
        return proposed_album_name
    else:
        return None
    
    
def rename_album(album_name, print_only=False):
    for name in list(os.listdir()):
        album_name1 = album_name + " "
        idx = name.find(album_name1)
        is_image = len(name)>4 and (name[-4:] in [".png", ".jpg", "jpeg"])
        if (idx != -1) and not is_image:
            start = idx
            end = idx + len(album_name1) + 5
            number = name[end-3:end]
            
            # Assert if something wrong
            if number[-1] != " ":
                continue
            try:
                num = int(number[:-1])
            except ValueError:
                continue
                
            new_name = number + name[:start] + name[end:]
            rename_text = "Rename:"
            if not print_only:
                os.rename(name, new_name)
                rename_text = "Renamed:"
            idx = max(name.find(new_name[:2]), 0)
            print("{:<8} {}".format(rename_text, name))
            print("{:<8} {}{}".format("to:", " "*idx, new_name))



def ascend_lower(print_only=False):
    listdir = os.listdir()
    _, dirname = os.path.split(os.getcwd())
    #print(os.getcwd())
    album_name = try_extract_album_name_from_directory(listdir, dirname)
    if album_name is not None:
        print("-------------------------------------------------")
        print("Found", album_name, "in", os.getcwd())
        rename_album(album_name, print_only)
        return True
    found_any = False
    for path in listdir:
        if os.path.isdir(path):
            os.chdir(path)
            found = ascend_lower(print_only)
            if found:
                found_any = True
            os.chdir("..")
    return found_any

def main():
    print("All bandcamp album songs are renamed recursively from current working directory.")
    print("(y = yes;    n = no;    p = print changes without renaming;    h = help)")
    print("Proceed? [y/n/p]")
    x = input()
    print_only = x in ["p", "P"]
    if x in ["y", "Y"] or print_only:
        if print_only:
            print("Folloing would be renamed:")
        else:
            print("Renaming:")
        found_any = ascend_lower(print_only)
        if not found_any:
            print("Did not find any albums, does your album directory name contain the album name?")
            print("For example:")
            print("    Let's say the album's name is \"AlbumName\".")
            print("    Let's say one of your songs is \"SomeArtis - AlbumName - 02 SongName.m4a.")
            print("    Directory must be named somethig like:") 
            print("        \"ReleaserName - AlbumName\" or \"BlahBlahAlbumNameBlah\".")
    elif x in ["n", "N"]:
        print("Ok, not doing it then.")
    else:
        print(help_text)

main()
