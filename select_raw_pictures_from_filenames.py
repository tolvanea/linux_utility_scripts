#!/usr/bin/python3
help_text = """
Copies RAW files by using set of JPGs that you have hand-selected.

DEPRECATED:
    - Use 'select_raw_pictures.py' instead.
    - Naive, also fails if images are renamed.
    - Purposefully not documented in readme.

Problem: Your image viewer is low with RAW-images but fast with JPGs. You wish to work with JPGs
when dropping out images not worth saving, but you wish work with raws in post-processing phase.
Solution:
    1. Select the images you want to save using JPGs
    2. Use this script to get the corresponding RAWs
    3. Do the editing with RAWs

Usage:
    Be in path where you have selected the best jpgs. Give raw-image folder path as argument for
    this script. This script will copy all corresponding raws to the newly created folder "raws".

Usage example:
    cd /path/to/my_chosen_JPGs
    select_raw_pictures.py /path/to/all_my_raws/

Notes:
    - Pro-tip 1:
      To get the sd-card's path copied easily on clip board, navigate into the RAW-folder
      using your file browser and press "ctrl + L". If that shortcut is supported,
      the path becomes copy-pasteable.

    - Pro-tip 2:
      To directly copy from the most recent directory in you SD-card, use command:
          select_raw_pictures.py $(ls -td /your_SD_card/DCIM/*/ | head -1)
"""

# Todo, use has or something to go over renames.

import os
import sys
import shutil

def copy_images(raw_path, jpg_path, raws_destination_path):
    print("Copying from", os.path.abspath(raw_path), "to", os.path.abspath(jpg_path))
    jpgs = {}  # Keys: filenames without extension, values: filepaths
    for f in sorted(os.listdir(jpg_path)):
        f_path = os.path.join(jpg_path, f)
        if os.path.isfile(f) and len(f) > 5 and ((f[-4:]==".JPG") or (f[-4:]==".jpg")):
            filename_without_ext = os.path.splitext(f)[0]
            jpgs[filename_without_ext] = f_path

    raws = {}  # Keys: filenames without extension, values: filepaths
    for f in sorted(os.listdir(raw_path)):
        f_path = os.path.join(raw_path, f)
        if os.path.isfile(f_path) and len(f) > 5 and ((f[-4:]==".ARW") or (f[-4:]==".RAW")):
            filename_without_ext = os.path.splitext(f)[0]
            raws[filename_without_ext] = f_path

    copied_something = False
    for jpg in jpgs.keys():
        if jpg in raws:
            copied_something = True
            print("copying", os.path.basename(raws[jpg]), "to company", os.path.basename(jpgs[jpg]))
            # unlike copy(), copy2() preserves metadata
            shutil.copy2(raws[jpg], raws_destination_path)

    if not copied_something:
        print("Nothing copied. There was no jpg and raw images with matching names.")


def main():
    args = sys.argv
    print(args)
    if len(args) != 2:
        print("Give one argument, which is path of raw folder. You must be in jpg folder.\n")
        print(help_text)

    if args[1] in ["--help", "help", "-h,"]:
        print(help_text)

    raw_dir = "./raws"
    os.makedirs(raw_dir, exist_ok=True)

    copy_images(args[1], "./", raw_dir)

main()
