#!/usr/bin/python3
help_text = """
Copies RAW files by using set of JPGs that you have hand-selected.

When to use?
    > Your camera's SD card has 500 JPGs and 500 RAWS.
    > Your image vieweing program is slow displaying RAWs but fast displaying JPGs.
    > I takes eternity to move 500 RAWs from the SD card to a computer.
    > You prioritise fast experience, so you copy the JPGs into a folder, and work with them.
    > Out of those 500 JPGS, you select 30 JPGs worth saving
    > You want to do post-processing with RAWS, but that would recuire getting 30 RAW-files by hand.
    > Doing the same thing 30 times is too manual.
Solution :
    > Go to the folder containg the 30 JPGs you selected.
    > Use this script to copy the corresponding 30 RAWs from the SD card.
    > Copied images are found from newly created folder "raws".
    > Do the post-processing with the RAWs.

Usage example:
    cd /path/to/my_chosen_JPGs
    select_raw_pictures.py /path/to/all_my_raws/

Notes:
    - Your JPGs can be renamed: this script uses exif-metadata to match the JPGs with corresponding
      RAWs. So you can safely use the script 'rename_images.py' before copying the raws.

    - This script works with following raw extensions: ".ARW" ".RAW" and ".DNG".

    - Pro-tip 1:
      To get the sd-card's path copied easily on clip board, navigate into the RAW-folder
      using your file browser and press "ctrl + L". If that shortcut is supported,
      the path becomes copy-pasteable.

    - If you want to search for raws from the whole sd-card, that is supported too.
      That is, if you have raws in multiple directories like the following:
        "/media/you/disk/DCIM/day-1/"
        "/media/you/disk/DCIM/day-2/"
        "/media/you/disk/DCIM/day-3/"
      you can pass the master directory directly to search all subdirectories
        select_raw_pictures.py "/media/you/disk/DCIM/"

    - This script is tested only with sony metadata on Linux.

    - Can't install 'py3exiv2' library? You can also use 'select_raw_pictures_from_filenames.py',
      which is almost the same, but uses filenames to match RAWs instead of time-metadata.

    - If images have been taken with rapid fire, this script can not tell them apart from
      metadata (time and brightness), and all matching RAWs are copied.

"""

import os
import sys
import shutil

try:
    # INSTALLATION OF PYTHON EXIF READER ON UBUNTU:
    # sudo apt-get install libboost-python-dev libexiv2-dev python3-pip
    # pip3 install py3exiv2
    import pyexiv2
except ModuleNotFoundError as e:
    print("py3exiv2 is not installed. Install it with:")
    print("    sudo apt-get install libboost-python-dev libexiv2-dev python3-pip")
    print("    pip3 install py3exiv2")
    exit()

from datetime import datetime


def copy_images(raws_directory_path, jpgs_directory_path, raws_destination_path):
    print(
        "Copying from\n    {}\nto\n    {}\n"
        .format(os.path.abspath(raws_directory_path), os.path.abspath(raws_destination_path)))

    # Filepaths to each jpg and raw image. (Don't worry, extension "jpg" is case insensitive)
    selected_jpg_files = get_all_files_that_have_extension(jpgs_directory_path, [".jpg"])
    all_raw_files      = get_all_images_on_sd_card(raws_directory_path, [".arw", ".raw", ".dng"])

    selected_jpg_hashes, corrupt_jpgs = get_hashes(selected_jpg_files)
    all_raw_hashes,      corrupt_raws = get_hashes(all_raw_files)
    corrupt_images = corrupt_jpgs + corrupt_raws

    some_images_copied = False
    # Copy those RAW images that have matchig hash with JPGs.
    # This logic is ugly and complicated only because it handles the rare hash collisions
    for (jpg_hash, jpg_paths) in selected_jpg_hashes.items():


        if jpg_hash in all_raw_hashes:
            some_images_copied = True
            duplicate_str = ""

            # Handle hash collisions
            raws_for_hash = all_raw_hashes[jpg_hash]
            if len(raws_for_hash) > 1: # Metadata hash collision -> try to resolve it from filename
                possible_duplicate, raws_for_hash = handle_hash_collision(raws_for_hash, jpg_paths)
                if possible_duplicate:  # Resolution failed
                    duplicate_str = " (Unsure)"
            else:  # Normal case
                possible_duplicate = False

            for raw_path in raws_for_hash:  # Usually only one iteration if no hash collisions
                if len(jpg_paths) == 1:
                    # Usually 1 jpg-image per hash. If not, all raws will be copied nevertheless
                    jpg_name = os.path.basename(jpg_paths[0])
                else:
                    # Show all jpgs that are linked to hash of current jpg
                    jpg_name = " or ".join([os.path.basename(path) for path in jpg_paths])

                raw_name = os.path.basename(raw_path)


                print(f"    Copying {raw_name} to company {jpg_name}{duplicate_str}")
                # unlike copy(), copy2() preserves metadata
                shutil.copy2(raw_path, raws_destination_path)
        else:
            print(f"Did not find corresponding raw for {os.path.basename(jpg_paths[0])}")

    give_error_messages_for_user(
        some_images_copied,
        selected_jpg_files,
        all_raw_files,
        jpgs_directory_path,
        raws_directory_path,
        corrupt_images
    )

# If metadata for capture time and brightness is exactly the same with 2 images, hash collision
# happens, and it need to be handled with extra care. If images have been renamed, there is no way
# to tell what raw corresponds to given jpg, so all matching raws are copied, and the job is left
# to user figure out what is correct one. Usually only up to few images may have identical
# metadata, so the user does not need to watch through many images in case of conflict.
#
# Return:
#   possible_duplicate:     Flag that tells if following argument is properly determined
#   raws_for_hash_output:   If possible_duplicate = false, then all raws match the jpgs,
#                           If possible_duplicate = true, then also non-matching raws are included
def handle_hash_collision(raws_for_hash, jpg_paths):
    jpg_name = os.path.basename(jpg_paths[0])
    jpg_name_without_ext = os.path.splitext(jpg_name)[0]
    raw_names = [os.path.basename(name) for name in raws_for_hash]
    raw_names_without_ext = [os.path.splitext(name)[0] for name in raw_names]

    assert (len(raws_for_hash) > 1)  # Multiple raws for hash exists

    raws_for_hash_output = []

    # Special happy case: If there are as many raws as jpgs, we can just copy them all
    if len(raws_for_hash) == len(jpg_paths):
        return False, raws_for_hash

    for jpg_path in jpg_paths:
        jpg_name = os.path.basename(jpg_path)
        jpg_name_without_ext = os.path.splitext(jpg_name)[0]

        if jpg_name_without_ext in raw_names_without_ext:
            # Hash conflict can be solved for this jpg because its name mathces a raw image
            correct_raw = raw_names_without_ext.index(jpg_name_without_ext)
            raws_for_hash_output.append(raws_for_hash[correct_raw])
            possible_duplicate = False
        else:
            # Uh oh, there was one jpg that could not be solved --> return all raws that may match
            raws_for_hash_output = raws_for_hash
            possible_duplicate = True
            break

    if len(jpg_paths) == 1:
        jpg_name = os.path.basename(jpg_paths[0])
    else:
        jpg_name = [os.path.basename(path) for path in jpg_paths]

    if possible_duplicate:
        # Hash conflict can not be solved. Copy all possible images that match
        print("WARNING: Colliding time stamp!")
        print(f"    Can not tell which raw corresponds to {jpg_name}.")
        print(f"    All conflicting RAWs are copied, so there are also raws you did not ask for.",)
        print(f"    These are copied:", raws_for_hash_output)

    return possible_duplicate, raws_for_hash_output

# Get images.
# Input path can be either directory that contains jpgs, or it can be directory that contains
# directories of jpgs. That is, for sony cameras, it can match either the whole sd-card
# "/media/alpi/disk/DCIM/" or date specific directory "/media/alpi/disk/DCIM/11030103/".
# Input:
#   path_to_images: string      Filepath. E.g. "/media/you/disk/DCIM" or "/media/you/disk/DCIM/day-2"
#   extensions: list<string>    Extensions to search for,  case in-sensitive.  E.g. [".jpg"].
# Return:
#   files: list<filepath>       Files that have matching extension
def get_all_images_on_sd_card(path_to_images, extensions):
    images = get_all_files_that_have_extension(path_to_images, extensions)

    if images != []:
        # Images found from given directory -> use it directly.
        return images
    else:
        # If no images are found from given direcoty, try looking into subdirectories.
        files = []
        for f in sorted(os.listdir(path_to_images)):
            path = os.path.join(path_to_images, f)
            if os.path.isdir(path):
                imgs = get_all_files_that_have_extension(path, extensions)
                files.extend(imgs)
        return files

# Get images in specific folder
# Input:
#   path_to_images: string      Filepath to images. E.g. "/media/you/disk/DCIM/day-2/"
#   extensions: list<string>    Extensions to search for,  case in-sensitive.  E.g. [".jpg"].
# Return:
#   files: list<filepath>       Files that have matching extension
def get_all_files_that_have_extension(path_to_images, extensions):
    files = []
    for f in sorted(os.listdir(path_to_images)):
        filepath = os.path.join(path_to_images, f)
        file_ext = os.path.splitext(filepath)[1]
        for matching_ext in extensions:
            # Case insensitive extension: Both "jpg" and "JPG" match
            # Can be with or without dot: Both ".jpg" and "jpg" match
            if file_ext.lower().endswith(matching_ext.lower()):
                files.append(filepath)
    return files

# A hash is calculated for each image in input. Hash is formed from metadata so that RAW and JPG
# files have the same hash. This function is used to find what RAWs correspond to selected JPGs.
# Input:
#   image_files: list<filepath>
#       Can be obtained with 'get_all_files_that_have_extension'
# Return:
#   hashes: map<hash, list<filepath>>
#       Each "hash" correspond to an image in input list. There is "list<filepath>" for each hash,
#       beause hash collisions may happen, and multiple images have same hash. But at least in 99%
#       of cases, you can be sure that 'list<filepath>' is list with length of 1.
#   corrupt_images: list<filepath>
#       Sometimes the camera corrupts metadata, and it can not be read. This script can not know if
#       those files are important or not, and they are returned as a list. Those failed files are
#       later copied in another directory for user to decide if there were anything important.
def get_hashes(image_files):
    hashes = {}
    corrupt_images = []
    for filepath in image_files:
        metadata = pyexiv2.ImageMetadata(filepath)
        try:
            metadata.read()
            try:
                date = metadata["Exif.Photo.DateTimeOriginal"].value
            except KeyError:
                date = metadata["Exif.Image.DateTime"].value
        except Exception as e:
            print("    Could not read metadata of {}: ".format(filepath) + str(e))
            corrupt_images.append(filepath)
            continue

        # Brightness is quite random number, which is great for having somewhat unique a hash.
        # The brightness is, of course, same for both jpg and raw.
        try:
            brightness = metadata["Exif.Photo.BrightnessValue"].value
        except KeyError:
            print("    Could not read brightness of {}: ".format(filepath))
            continue # Brightness not available

        h = hash((date, brightness))
        if h in hashes:  # Hash collision! Multiple images per hash. Rare but possible
            hashes[h].append(filepath)
        else:
            hashes[h] = [filepath] # Happy route: one image per hash.


    return hashes, corrupt_images

def give_error_messages_for_user(
    some_images_copied,  # bool
    selected_jpg_files,  # list<filepath>
    all_raw_files,       # list<filepath>
    jpgs_directory_path, # filepath
    raws_directory_path, # filepath
    corrupt_images       # list<filepath>
):
    if not some_images_copied:
        print("Nothing copied. There was no raw images that had a matching name with these jpgs.")
        print("So no overlap between contents of", jpgs_directory_path, "and", raws_directory_path)

        if len(selected_jpg_files) == 0:
            print("No jpgs found from", jpgs_directory_path)
        elif len(all_raw_files) == 0:
            print("No raws found from", raws_directory_path)
        else:
            print("First few of your jpgs are for example:")
            for jpg in selected_jpg_files[:min(3, len(selected_jpg_files))]:
                print("   ", jpg)
            if len(selected_jpg_files) > 3:
                print("   ", "...")

            print("First few of your raws are for example:")
            for raw in all_raw_files[:min(3, len(all_raw_files))]:
                print("   ", raw)
            if len(all_raw_files) > 3:
                print("   ", "...")

    if len(corrupt_images) > 0:
        print("\nThere were files of which metadata could not be read. So that you can sort the")
        print("images that are left out, they are copied under new folder:")
        corrupt_destination_path = "./could_not_read_metadata"
        os.makedirs(corrupt_destination_path, exist_ok=True)
        for corrupt_image in corrupt_images:
            print("    Copying", corrupt_image, "to", corrupt_destination_path )
            shutil.copy2(corrupt_image, corrupt_destination_path)



def main():
    args = sys.argv
    if len(args) != 2:
        print("Give one argument, which is path of raw folder. You must be in jpg folder.\n")
        print(help_text)

    elif args[1] in ["--help", "help", "-h,"]:
        print(help_text)

    else:
        raw_dir = "./raws"
        os.makedirs(raw_dir, exist_ok=True)

        copy_images(args[1], "./", raw_dir)

main()
