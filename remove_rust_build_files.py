#!/usr/bin/python3
# Deprecated, use "cargo sweep -r -t 90 ." instead

"""
Removes all auxiliary rust files (i.e. 'target/') to save space on hard drive.
This script works recursively for current working directory.
"""

import os
import shutil


def is_rust_folder(dirs, files):
    return (("src" in dirs) and ("target" in dirs) and ("Cargo.toml" in files))

def ascend_lower(path, dry_run=False):
    for root, dirs, files in os.walk(os.path.abspath(path)):
        if is_rust_folder(dirs, files):
            target_dir = os.path.join(root, "target")
            if dry_run:
                print("Found", target_dir)
            else:
                print("Removing", target_dir)
                shutil.rmtree(target_dir)

def main():
    print("All auxillary rust build files are removed recursively from current")
    print("working directory? Select N for a dry run. [Y/N]")
    x = input()
    if x == "Y" or x == "y":
        print("Removing following files:")
        ascend_lower(os.getcwd())
    else:
        print("These directories would be removed:")
        ascend_lower(os.getcwd(), dry_run=True)

main()
