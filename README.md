# linux_utility_scripts

This repo contains some useful scripts that automate some singular tasks.

They are licenced under MIT or public domain.

List:
* `change_exif_time.py`: Changes exif time stamp of pictures. Used to fix time stamps afterwards when camera have had wrong clock settings.
* `fimfic_to_audiobook.py`: Converts fanfictions downloaded from fimficton.net to to audiobooks using modern AI-based text-to-speech (TTS). Pretty nice AI-TTS reading.
* `hevcify.py`: Convert all videos in current directory to space-efficient HEVC format. File size can reduce to one third of original without loss of quality. This uses `ffmpeg` on background.
* `goprocess.py`: Convert gopro videos to HEVC. Also rename filename so that it includes date.
* `merge_videos`: Merge two video files side by side. This is made for video lectures: merge both white board and slides into a single video.
* `pdf_ocr.sh`: Convert a scanned pdf so that the text can be selected.
* `ping_monitor.py`: Ping some server regularly and draw live graph of latencies. Compare graphs of two separate pings commands. Used to measure mullvad vpn: one ping is made with mullvad tunneling on, and another one without it. The plotted graph makes it easier to detect if connection problems arise from overloaded mullvad servers.
* `print_image_metadata.py`: Print all metadata of an image, also expose and decrypt some hidden metadata from manufacturer.
* `remove_rust_build_files.py`: Remove temporal rust build directories recursively from current directory. This cleans up a lot of space. Though currently also now exists also tool `cargo-sweep` that does the same job.
* `rename_bandcamp.py`: Rename bandcamp songs recursively in current directory. For example, song "artist - album - No. Song Name.m4a" gets renamed to "No. artist - Song Name.m4a", where "No." is the number of track. Not essential, but pleases mind.
* `rename_images.py`: If you have pile of images named like "DCS1234.JPG", this script renames them like by date like "2022-04-22_13.JPG". Very handy!
* `rename_yle_puhe.py`: Rename podcasts from _Yle Areena_ so that date appears in the beginning of name. Not essential in any way, but nice for organizing.
* `rsync_csc.sh`: Synchronize files between ssh server and home pc
* `select_raw_pictures.py`: If you have set of hand-selected JPGs in current directory, this script copies the corresponding RAWs from memory card into to accompany the JPGs. Very useful in image processing pipeline!
* `summarize_op_csv.py`: Summarize a csv exported from Osuus Pankki -bank transfers. Summs up expenses/incomes by recipient/sender.
* `summarize_paypal_csv.py`: Summarize a csv that is exported from PayPal. Tells how much money is spent to what.
* `wacom_intuos`: Configure wacom tablet to be suited for hand writing. Reduce draw delay significantly by disabling line averaging. Also tweak pressure sensitiveness to match real pen more closely.
* `yt-dl`: Download youtube playlists to music. Add thumbnail and fetch artist information from channel data. This uses `youtube-dl` on background.
* `zshrc`: Many small utility tools that remove friction from terminal workflow. There are many aliases to shell one-liners, which make for example easy searching in filesystem.

| Script         | My use frequency | Few words |
| :------------- | -----------: | -----------: |
| `change_exif_time.py`         | 1/year  | Fix incorrect timestamps of pictures |
| `fimfic_to_audiobook.py`      | 4/year  | Generate pretty-ok audiobooks from text |
| `hevcify.py`                  | 4/year  | I compress most of my videos with this |
| `goprocess.py`                | 4/year  | I compress most of my videos with this |
| `merge_videos`                | 0/year   | Render two videos side-to-side |
| `pdf_ocr.sh`                  | 2/year  | Make non-searchable PDF searchable |
| `ping_monitor.py`             | 1/year  | Check if internet connection is laggy |
| `print_image_metadata.py`     | 5/year  | Find out picture metadata, including hidden |
| `remove_rust_build_files.py`  | 3/year  | Recommended to use `cargo-sweep` instead |
| `rename_bandcamp.py`          | 10/year | Clear up song names downloaded from bandcamp |
| `rename_images.py`            | 50/year | Rename images by date, instead of "DSC01234" |
| `rename_yle_puhe.py`          | 1/year  | Rename podcasts downloaded from yle areena |
| `rsync_csc.sh`                | 10/year | Synchronize data on ssh computer |
| `select_raw_pictures.py`      | 50/year | Useful to fetch raw pictures for JPGs |
| `summarize_op_csv.py`         | 1/year  | Summarize transactions from OP-bank account |
| `summarize_paypal_csv.py`     | 1/year  | Summarize transactions from paypal account |
| `wacom_intuos`                | 20/year  | Required to make wacom bearable! |
| `yt-dl`                       | 10/year | Download youtube playlists directly to music |
| `zshrc`                       | 50/year  | Handy tools for terminal use. Like ~/.bashrc |
