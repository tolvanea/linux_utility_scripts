#!/usr/bin/env python3
# This script modifies clock metadata of all images in current directory.
# That is, if you have taken images with wrong system time, you can modify the time
# metadata with this. This version of script puts the clock one hour back, but you can
# modify the time difference you need.
# Also, currently only those pictures are processed that are taken with sony a6400.

import pyexiv2
import sys
from datetime import datetime
import os

# Only pictures taken with model below are processed. If you do want to include all pictures,
# take out the if-branch from code.
camera_model = "ILCE-6400"

# Number of hours to but clock backwards
hours_to_reverse = 1

# Modify these date exif fields
date_keys = ["Exif.Image.DateTime", "Exif.Photo.DateTimeDigitized", "Exif.Photo.DateTimeOriginal"]

# -----------

# Get jpg-images in current directory as list. E.g. returns list
#       ["DSC45678.JPG", "IMG01234.JPG"]
def get_images():
    path_to_images = "."
    extensions =  [".jpg", ".jpeg", ".arw", ".dng"]
    files = []
    for f in sorted(os.listdir(path_to_images)):
        filepath = os.path.join(path_to_images, f)
        file_ext = os.path.splitext(filepath)[1]
        for matching_ext in extensions:
            # Case insensitive extension: Both "jpg" and "JPG" match
            # Can be with or without dot: Both ".jpg" and "jpg" match
            if file_ext.lower().endswith(matching_ext.lower()):
                files.append(filepath)
    return files

# One hour time difference. Used to put the time one hour back.
# (Sorry, currently I have no internet, and this is the only way I can come up to make an hour span)
def hour_difference():
    date1 = datetime.strptime('2022:12:01 02:00:00', '%Y:%m:%d %H:%M:%S')
    date2 = datetime.strptime('2022:12:01 03:00:00', '%Y:%m:%d %H:%M:%S')
    return date2 - date1

# (Sorry, currently I have no internet, and this is the only way I can come up to format date)
def format_date(date):
    return '{:02d}:{:02d}:{:02d} {:02d}:{:02d}:{:02d}'\
        .format(date.year, date.month, date.day, date.hour, date.minute, date.second)

def modify_metadata():
    for image_filename in get_images():
        metadata = pyexiv2.ImageMetadata(image_filename)
        metadata.read()
        if metadata["Exif.Image.Model"].raw_value == camera_model:
            for date_key in date_keys:

                date_str = metadata[date_key].raw_value
                date = datetime.strptime(date_str, '%Y:%m:%d %H:%M:%S')

                new_date = date - hour_difference() * hours_to_reverse
                new_date_str = format_date(new_date)

                print(
                    "Modified date", date_str, "to", new_date_str,
                    "in file", image_filename, "("+camera_model+")."
                )

                metadata[date_key].raw_value = new_date_str
            metadata.write()

modify_metadata()
