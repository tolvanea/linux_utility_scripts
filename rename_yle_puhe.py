# nimeä uudelleen ylen ohjelmia siten, että lopun päivämäärä tulee ensimmäiseksi
# esim.:
#     Rakennelmia_hiljaisuudesta-2016-08-28.flv
# --> 2016-08-28-Rakennelmia_hiljaisuudesta.flv
# toimii .mp3 tai .flv päätteisille tiedostoille

# todo ffmpg .flv jos ei .mp3 valmiiksi
import os

for file0 in os.listdir():
    ext = file0[-4:] # extension
    
    if (os.path.isfile(file0) and (ext==".mp3" or ext==".flv")):
        
        found = True
        
        # downloaded multiple times:  "filename(1).mp3"
        if (file0[-7] == "(" and file0[-5] == ")"):
            file = file0[0:-7] + ext
        else:
            file = file0
        # times are ISO-standard-format

        
        # Avaruusromua: Äärirajoille asti!-2017-12-10T10:00.flv
        if (file[-10] == 'T') and (file[-7] == ':'):
            name = file[0:-21]
            date = file[-20:-4]
            
        # Avaruusromua: Rentoudu kerrankin kunnolla!-2016-11-05T22:05:00+02:00.flv
        elif (file[-10] == '+') and (file[-19] == 'T') and (file[-16] == ':'):
            name = file[0:-30]
            date = file[-29:-4]
            
        # Rakennelmia_hiljaisuudesta-2016-08-28.flv
        elif (file[-7] == '-') and ((file[-10] == '-') and (file[-14] == '2')):
            name = file[0:-15]
            date = file[-14:-4]
            
        # Jari-Sarasvuo-Me-kaikki-tarvitsemme-terapiaa-2018-12-17-v25750866.mp3
        elif (file[-14] == '-') and (file[-13] == 'v'):
            name = file[0:-25]
            date = file[-24:-14]
        
        # already re-named before
        elif (file[4] == '-') and (file[7] == '-'):
            found = False
        
        else:
            print("Unknown format!", file)
            found = False
        
        
        if found:
            new_name = date + "-" + name + ext
            print("Rename", new_name)
            os.rename(file, new_name)
